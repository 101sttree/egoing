import React,{useState} from 'react';

function Counter(){
    const [number, setNumber] = useState(10);
    const onIicrease = () =>{
       setNumber(prevNumber => prevNumber + 1)
    }
    const onDecrease =()=>{
        setNumber(prevNumber => prevNumber - 1)
    }
    return(
        <div>
            <h1>{number}</h1>
            <button onClick={onIicrease}>+1</button>
            <button onClick={onDecrease}>-1</button>
        </div>
    );
}

export default Counter;