import React from 'react';

function User({users}){
    return(
        <div>
            <span>{users.username}</span>
            <span>{users.email}</span>
        </div>
    );
}

function UserList({users}){

    return(
        <div>
            {users.map((user, index) => (<User users={user}/>))}
        </div>
    );
}

export default UserList;