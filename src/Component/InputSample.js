import React, { useState, useRef } from 'react';

function InputSample(){
    const [inputs, setinputs] = useState({
        name : '',
        nickname : ''
    });
    const nameInput = useRef();
    //usesteat를 통해 변수 inputs에 name과 nickname를 가진 객체 할당
    //자바스크립트의 객체는 키와 값을 가진 프로퍼티들의 집합
    const{name,nickname} = inputs;
    /* 비구조화 할당으로서 = 다음에 오는 
    객체에서 값을 추출하여 변수선언과 동시에 할당
    리액트에서 변수를 선언하려면 {}로 감싸야 하니까?
    아래에서 사용하기 위해 선언해놓음? */

    const onChange = (e) =>{
        const{value, name} = e.target;
        /* 이벤트 타겟에 있는 값을 가져와 현재 블록에서 사용가능한
        변수로 할당 */
        setinputs({
            ...inputs,
            [name] : value
            /* inputs[name] = value 와 같은 식임
            이벤트로 받은 name가 키인 프로퍼티의 값을 
            이벤트로 받아온 value 로 할당 */
        });
    };
    const onReset = () =>{
        setinputs({
            name : '',
            nickname : ''
        })
        nameInput.current.focus();
    };
    return(
        <div>
            <input placeholder="이름"   
                name="name"     
                onChange={onChange} 
                value={name}
                ref={nameInput}
            />
            <input 
                placeholder="닉네임" 
                name="nickname" 
                onChange={onChange} 
                value={nickname}
            />
            <button onClick={onReset}>초기화</button>
            <div>
                <b>값:</b>
                {name}({nickname})
            </div>
        </div>
    );
}

export default InputSample;