import React from 'react';

function CreateUser({username,email,onChange,onCreate}){
    /* 
        부모 컴포넌트인 App.js에서 4개의 props를 비구조 할당을 통해 받아옴
        username,email,onChange,onCreate
    */
    return(
        <div>
            <input
                name="username"
                placeholder="계정명"
                onChange={onChange}
                value={username}
            />
            {/* 
               입력값이 변동될때 App.js에서 매개변수로 받아온  onCreate 함수가 실행
            */}
            <input
                name="email"
                placeholder="이메일"
                onChange={onChange}
                value={email}
            />
            {/* 
               입력값이 변동될때 App.js에서 매개변수로 받아온  onCreate 함수가 실행
            */}
            <button onClick={onCreate}>등록</button>
            {/* 
               버튼이 클릭되었을 때 App.js에서 매개변수로 받아온  onCreate 함수가 실행
            */}
        </div>
    );
}

export default CreateUser;