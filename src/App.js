import React, {Component, useRef, useState} from 'react';
import CreateUser from './Component/CreateUser';
import UserList from './Component/UserList';

function App(){
	
	const[inputs, setinputs] = useState({
		username : '',
		email : ''
	});
	/* 
		비 구조화 할당 작업
		변수값 할당을 간단하게 함
	 */
	
	const{username, email} = inputs;
	/* 
		위에서 선언한 inputs의 키와 값을 순서대로
		두 변수 username과 email에 할당
	*/
	
	const onChange = e => {
		const{name, value} = e.target;
	/* 
		CreateUser 컴포넌트에 전달할 onChange 작성
		이벤트 객체 e를 인자로 받아 사용
		e.target을 통해 input 태그의 전달값에 접근 가능
		input 태그의 값을 순서대로 name과 value에 할당
	*/		
		
		setinputs({
			...inputs,
			[name] : value
		});
		/* 
			useState 통해 할당된 setinputs를 사용하여
			inputs의 값 수정
			...inputs 는 spread 연산자로서 해당 배열이 전부 출력된다.
			[name] : value 는 e.target 를 통해 받아온 name에 해당하는 키의 값을
			받아온 value 로 할당한다. 
		*/
	};
	const [users, setUsers]=useState([
        {
            id : 1,
            username : 'a',
            email : 'aaa@naver.com'            
        },
        {
            id : 2,
            username : 'b',
            email : 'bbb@naver.com'            
        },
        {
            id : 3,
            username : 'c',
            email : 'ccc@naver.com'            
        }
    ]);
	/* 
		users라는 변수에 배열을 할당한다.
	*/
	const nextId = useRef(4);
	/* 
		useRef()는 컴포넌트 안에서 조회 및 수정할수 있는 변수를 관리할 수 있다.
		인자로 들어간 값은 useRef()가 할당된 변수의 기본값이 된다.
		현재는 항목에 사용될 고유 id 용으로 선언
	*/
	const onCreate = ()=>{
		const user = {
			id : nextId.current,
			username,
			email
		};
	/* 
		onCreate라는 변수에 무명함수를 할당.
		함수에서는 user 배열을 생성.
		id의 값은 useRef()로 선언된 nextId값을 할당.
	*/	
		/* spread 연산자 사용 */
		/* setUsers([...users,user]); */
		/* 
			spread 연산자를 사용하여 기존 배열인 users에 새로운 키와 값이 할당된
			user을 결합
		*/
		/* concat 함수 사용 */
		setUsers(users.concat(user));
		setinputs({
			username : '',
			email : ''
		});
		/* 
			concat 함수는 해당 배열에 인자에 해당하는 배열을 결합한 배열을 반환
			spread 와 concat 모두 기존 배열의 값에 영향을 미치지 않는다.
		*/
		nextId.current += 1;
		/* 
			고유한 키 값을 부여하기 위해 onCreate 함수가 작동할 때 마다
			값 증가, 기본값이 4인 이유는 기존 글이 3개이기 때문이다.
		*/
	};
	
	return(
		<>
			<CreateUser
				username={username}
				email={email}
				onChange={onChange}
				onCreate={onCreate}
			/>
			<UserList users={users}/>
		</>
	); 
	
}
export default App;

